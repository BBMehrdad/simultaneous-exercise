# Simultaneous

## Running the shared javascript webpack hot reload server

From within the shared directory:

    $ npm install
    $ npm start

## Running JIRA Server

From within the jira-server directory:

    $ atlas-debug --jvmargs '-Darijea.dev.mode'

## Running JIRA Cloud

From witin the jira-cloud directory:

    $ npm install
    $ npm start

## Notes on getting the shared javascript to load in a JIRA Cloud dev server. 

For the JIRA Cloud add-on to successfully load the javascript from the webpack hot reload server, it needs to be
requested both via https. You will need to create an additional localtunnel.me connection on port 3001 and 
update the a script tag to point to the tunnel.

    $ npm install -g localtunnel
    $ lt --port 3001 

Copy that tunnel address and update the script reference at the bottom of jira-cloud/views/layout.hbs to point to 
/static/cloud.js of the localtunnel.me address. i.e. https://uaiw.localtunnel.me/static/cloud.js

If you run in to any trouble, please don't hesitate to hit me up on twitter at @edave.
