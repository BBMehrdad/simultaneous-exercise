import gulp from "gulp";
import gutil from "gulp-util";
import webpack from "webpack";
import webpackConfig from "./webpack.config"
import path from "path";
import { src_root, containers_src } from "./app_vars.js";

gulp.task("default", ["build"]);

// Production build
gulp.task("build", function(callback) {
    // modify some webpack config options
    var config = Object.create(webpackConfig);
    config.plugins = [
        new webpack.DefinePlugin({
            "process.env": {
                // This has effect on the react lib size
                "NODE_ENV": JSON.stringify("production")
            }
        }),
        new webpack.optimize.DedupePlugin(),
        new webpack.optimize.UglifyJsPlugin()
    ];

    config.resolve = {
        alias: {
            "dev/DevTools.js": path.join(containers_src, "blank.js"),
            "stores/configureStore.js": path.join(src_root, "stores", "configureStore.js")
        }
    };

    config.devtool = "none";

    config.output = {
        path: require("path").resolve("./lib"),
        filename: "index.js"
    };

    config.module.loaders[0].loaders = ['babel'];

    // run webpack
    webpack(config, function(err, stats) {
        if(err) throw new gutil.PluginError("webpack:build", err);
        gutil.log("[webpack:build]", stats.toString({
            colors: true
        }));
        callback();
    });
});