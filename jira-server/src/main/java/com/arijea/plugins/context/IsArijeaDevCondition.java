package com.arijea.plugins.context;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class IsArijeaDevCondition implements Condition {

    private boolean useInDev = false;

    public void init(Map<String, String> params) throws PluginParseException {
        final String param = params.get("useInDev");
        useInDev = param != null && param.equals("true");
    }

    public boolean shouldDisplay(Map<String, Object> context) {
        if (System.getProperty("arijea.dev.mode") == null) {
            return !useInDev;
        } else {
            return useInDev;
        }
    }
}
